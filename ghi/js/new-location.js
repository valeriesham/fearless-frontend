// add event listener for when DOM loads
window.addEventListener('DOMContentLoaded', async () => {
// declare a variable that will hold the URL for the states API
const url = "http://localhost:8000/api/states/"

//fetch the url, use the await keyword
try {
    const response = await fetch(url)
//if the response is ok, get the data using the .json method, await it also
    if (response.ok){
        const data = await response.json()
        const selectTag = document.getElementById('state')

        for (let state of data.states){
            // Create an 'option' element
            const option = document.createElement('option')
            // Set the '.value' property of the option element to the
            // state's abbreviation
            option.value = state.abbreviation

            // Set the '.innerHTML' property of the option element to
            // the state's name
            option.innerHTML = state.name + "("+state.abbreviation+")"

            // Append the option element as a child of the select tag
            selectTag.appendChild(option)
        }
    }
}
    catch(e) {
        console.error('error', e);
}
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
    event.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
    },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
        formTag.reset();
        const newLocation = await response.json();
    }

    });
});
