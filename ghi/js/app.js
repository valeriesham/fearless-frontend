function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
      <div class="card shadow mb-4">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer">${new Date(starts).toLocaleDateString()} - ${new Date(ends).toLocaleDateString()}</div>
        </div>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        throw new Error('Response not ok');
      } else {
        const data = await response.json();

        const columnContainer = document.querySelector('.row')

        for (let i = 0; i < data.conferences.length; i++){
            const conference = data.conferences[i];
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);

            if (detailResponse.ok) {
                const details = await detailResponse.json();

                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url
                const starts = details.conference.starts
                const ends = details.conference.ends
                const location = details.conference.location.name
                const html = createCard(name, description, pictureUrl, starts, ends, location)

                const columnIndex = i % 3;
                const column = columnContainer.querySelectorAll('.col')[columnIndex]
                column.innerHTML += html

                }
            }
        }
    } catch (e) {
      // Figure out what to do if an error is raised
      const errorMessage = document.getElementById('error-message');
      errorMessage.textContent = 'An error occurred while fetching and processing the data. Please try again later.';
      errorMessage.style.display = 'block';
      console.error('error', e);
    }

  });
