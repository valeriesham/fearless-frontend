/*
1. The value of the options for the locations select tag should be the value of the "id" property of the location object.
(This is comparable to what you did on the last page with setting the value of the option to the "abbreviation" property.)

2. Notice that the JSON expects a numeric identifier for the location. You'll need to change the location list API to include the id property of locations.

3. Make sure you POST to the correct API URL.

4. Clear the data from the form when it succeeds.
*/

window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/locations/"

    try{
        const response = await fetch(url)

        if (response.ok){
            const data = await response.json()
            const selectTag = document.getElementById('location')

            for (let location of data.locations){
                const option = document.createElement('option')
                option.value = location.id
                option.innerHTML = location.name
                selectTag.appendChild(option)
            }
        }

        const formTag = document.getElementById('create-conference-form')
        formTag.addEventListener('submit', async event => {
            event.preventDefault()

            const formData = new FormData(formTag)
            const json = JSON.stringify(Object.fromEntries(formData));

            const conferenceURL = "http://localhost:8000/api/conferences/"
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-type': 'application/json'
                }
            }
            const response = await fetch(conferenceURL, fetchConfig)
            if (response.ok){
                formTag.reset()
                const newConference = await response.json()
                console.log(newConference)
            }
        })

    }

    catch(e) {
        console.error('error', e);
    }
});
