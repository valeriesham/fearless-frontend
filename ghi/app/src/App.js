import Nav from './Nav';
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'
import ConferenceForm from './ConferenceForm'
import AttendConferenceForm from './AttendConferenceForm'
import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom'

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="attendees/new" element={<AttendConferenceForm/>} />
          <Route path="attendees" element={<AttendeesList attendees={props.attendees}/>} />
          <Route path="locations/new" element={<LocationForm/>} />
          <Route path="conferences/new" element={<ConferenceForm/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
